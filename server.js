const express = require('express');
const path = require('path');
const recipe = require('./src/api/recipes.js');

const PORT = process.env.PORT || 3000;
const app = express();

const apiRouter = createApiDevRouter();
const pagesRouter = createPageRouter();

app.use('/static', express.static(path.join(__dirname, 'src/static')));
app.use('/api', apiRouter);
app.use('/', pagesRouter);
app.use('*', pagesRouter);

app.listen(PORT, () => {
    console.log('Listening on http://localhost:' + PORT);
});

function createPageRouter() {
    const router = express.Router();

    router.get('*', express.static(path.join(__dirname, 'src/static')));

    return router;
}

function createApiDevRouter() {
    const router = express.Router();

    router.get('/recipes', recipe.get);
    router.get('/recipe/:recipe', recipe.getOne);

    return router;
}
