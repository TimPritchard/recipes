import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './routes';

const app = document.getElementById('app');

function onUpdate() {
    // document.getElementById('page-container').scrollIntoView();
}

render(
    <Router onUpdate={onUpdate} history={browserHistory} routes={routes} />,
    app
);
