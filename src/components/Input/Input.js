import React from 'react';
import PropTypes from 'prop-types';

class MyInput extends React.Component {
    constructor(props) {
        super(props);

        this.changeValue = this.changeValue.bind(this);
    }

    changeValue(event) {
        event.preventDefault();
        const {value} = event.currentTarget;

        this.props.setValue(value);
    }

    render() {
        return (
            <input
                className=""
                type={'text'}
                onChange={this.changeValue}
                name={this.props.name}
                value={this.props.value} />
        );
    }
}

MyInput.propTypes = {
    setValue: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string
};

MyInput.defaultProps = {
    setValue: () => {},
    value: '',
    name: 'sample'
};

export default MyInput;
