import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import RecipeItem from '../RecipeItem/RecipeItem';

class RecipePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            recipe: {}
        };
    }

    componentDidMount() {
        const recipeParam = this.props.params.recipe;

        axios.get(`/api/recipe/${recipeParam}`)
            .then(({data}) => {
                this.setState({recipe: data});
            })
            .catch((e) => {
                console.log(e);
                this.setState({recipe: {error: 'not found'}});
            });
    }

    render () {
        const {recipe} = this.state;

        if (recipe.error) {
            return <div>{"Sorry, this recipe doesn't exist or may have been removed"}</div>;
        }

        return (
            <RecipeItem recipe={recipe} />
        );
    }
}

RecipePage.propTypes = {
    params: PropTypes.object
};

RecipePage.defaultProps = {
    params: {}
};

export default RecipePage;
