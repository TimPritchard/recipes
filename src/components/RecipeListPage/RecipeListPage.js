import React from 'react';
import _ from 'lodash';
import axios from 'axios';

import Input from '../Input/Input';
import RecipeItem from '../RecipeItem/RecipeItem';

class RecipeListPage extends React.Component {
    constructor(props) {
        super(props);

        this.prefix = 'recipe-list-page';
        this.state = {
            recipes: [],
            searchValue: ''
        };

        this.setSearchValue = this.setSearchValue.bind(this);
        this.drawListItem = this.drawListItem.bind(this);
    }

    componentDidMount() {
        axios.get('/api/recipes')
            .then(({data}) => {
                this.setState({recipes: data});
            })
            .catch((e) => {
                console.log(e);
                this.setState({recipes: [{error: 'Sorry, we currently have no recipes for you.'}]});
            });
    }

    setSearchValue(searchValue) {
        this.setState({searchValue});
    }

    drawListItem(recipe, index) {
        const {searchValue} = this.state;
        const lowerCaseName = recipe.name.toLowerCase();

        // TODO: filter by ingredients and cook time
        if (lowerCaseName.includes(searchValue.toLowerCase())) {
            return (
                <li key={index}>
                    <RecipeItem recipe={recipe} key={index} />
                </li>
            );
        }

        return null;
    }

    render () {
        const {recipes = []} = this.state;
        const recipesError = _.get(recipes, '[0].error');
        if (recipesError) {
            return <div>{recipesError}</div>;
        }

        return (
            <div className={this.prefix}>
                <div className={`${this.prefix}__filter`}>
                    <h2>Filter by name</h2>
                    <Input value={this.state.searchValue} setValue={this.setSearchValue} />
                </div>
                <div>
                    <h2>Recipes</h2>
                        <ul>
                            {recipes.map(this.drawListItem)}
                        </ul>
                </div>
            </div>
        );
    }
}

export default RecipeListPage;
