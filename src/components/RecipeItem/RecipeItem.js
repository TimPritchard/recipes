import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

class RecipeItem extends React.Component {
    constructor(props) {
        super(props);

        this.prefix = 'recipe-item';

        this.drawIngredient = this.drawIngredient.bind(this);
    }

    drawIngredient(ingredient, index) {
        return (
            <li key={index} className={`${this.prefix}__ingredient`}>
                {ingredient}
            </li>
        );
    }

    render () {
        const {recipe} = this.props;
        const {name, imageUrl, ingredients = [], cookingTime} = recipe;

        return (
            <Link to={`/recipe/${recipe.id}`} className={this.prefix}>
                <img src={imageUrl} />
                <h3>{name}</h3>
                <ul>
                    {ingredients.map(this.drawIngredient)}
                </ul>
                <p>{cookingTime}</p>
            </Link>
        );
    }
}

RecipeItem.propTypes = {
    recipe: PropTypes.object.isRequired
};

export default RecipeItem;
