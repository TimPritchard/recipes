import React from 'react';
import PropTypes from 'prop-types';

class PageWrapper extends React.Component {
    render() {
        return (
            <div className="page-wrapper">
                {this.props.children}
            </div>
        );
    }
}

PageWrapper.propTypes = {
    children: PropTypes.object.isRequired
};

export default PageWrapper;
