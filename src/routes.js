import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/reactApp';
import RecipeListPage from './components/RecipeListPage/RecipeListPage';
import RecipePage from './components/RecipePage/RecipePage';
import NoMatch from './components/Common/NoMatch';

export default (
    <Route path="/" component={App} >
        <IndexRoute component={RecipeListPage} />
        <Route path="recipe/:recipe" component={RecipePage} />
        <Route path="*" component={NoMatch} />
    </Route>
);
