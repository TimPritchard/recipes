const fs = require('fs');
const PATH = require('path');

const ROOT = '../../';
const RECIPE_JSON = PATH.resolve(__dirname, ROOT, 'recipes.json');

module.exports = {
    get: (req, res) => {
        const {recipes} = readFile();
        return res.status(200).json(recipes);
    },
    getOne: (req, res) => {
        const {recipe} = req.params;
        const {recipes} = readFile();
        const filteredRecipes = recipes.filter((recipes) => {
            return recipes.id === recipe;
        });

        if (filteredRecipes.length) {
            return res.status(200).json(filteredRecipes[0]);
        }

        return res.status(404).json({});
    }
};

function readFile() {
    const recipes = fs.readFileSync(RECIPE_JSON, 'utf8');

    return JSON.parse(recipes);
}
