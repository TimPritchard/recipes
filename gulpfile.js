const gulp = require('gulp');
const requireDir = require('require-dir');

requireDir('./tools', {recurse: false});

gulp.task('default', ['build:watch:scss', 'build:watch:app', 'server']);
