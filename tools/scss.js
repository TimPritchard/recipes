const PATH = require('path');

const gulp = require('gulp');
const changed = require('gulp-changed');
const plumber = require('gulp-plumber');

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemap = require('gulp-sourcemaps');

const ROOT = '../';

const SCSS_SRC_FOLDER = PATH.resolve(__dirname, ROOT, 'src/scss/');
const SCSS_SRC_FILES = PATH.resolve(SCSS_SRC_FOLDER, '**/*.scss');
const SCSS_SRC_FILE = PATH.resolve(SCSS_SRC_FOLDER, 'styles.scss');

console.log(SCSS_SRC_FILE);

const SCSS_BUILD_FOLDER = PATH.resolve(__dirname, ROOT, 'src/static/css/');

gulp.task('build:scss', function() {
    return gulp.src(SCSS_SRC_FILE)
        .pipe(changed(SCSS_BUILD_FOLDER, {extension: '.scss'}))
        .pipe(plumber())
        .pipe(sourcemap.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemap.write())
        .pipe(gulp.dest(SCSS_BUILD_FOLDER));
});

gulp.task('build:watch:scss', [ 'build:scss' ], function() {
    gulp.watch(SCSS_SRC_FILES, [ 'build:scss' ]);
});
