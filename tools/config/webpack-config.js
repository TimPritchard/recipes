const PATH = require('path');

const ROOT = '../../';
const SRC_FOLDER = PATH.resolve(__dirname, ROOT, 'src/');
const SRC_ENTRY_FILE = PATH.resolve(__dirname, ROOT, SRC_FOLDER, 'client.js');
const BUILD_FOLDER = PATH.resolve(__dirname, ROOT, 'src/static/js/');
const PUBLIC_PATH = '/js/';

const BUILD_FILE = 'app.js';

const webpackConfig = {
    entry: {
        app: SRC_ENTRY_FILE
    },
    output: {
        path: BUILD_FOLDER,
    publicPath: PUBLIC_PATH,
        filename: BUILD_FILE
    },
    resolve: {
        symlinks: false
    },
    devtool: 'inline-source-map',
    debug: true,
    bail: true,
    node: {fs: "empty"},
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                include: [
                    SRC_FOLDER
                ],
                loader: 'eslint-loader'
            }
        ],
        loaders: [
            {
                test: /\.js$/,
                include: [
                    SRC_FOLDER
                ],
                loader: 'babel',
                query: {
                    compact: false,
                    cacheDirectory: true
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    plugins: [
    ]
};

module.exports = webpackConfig;
